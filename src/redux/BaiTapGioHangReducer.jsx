// setup State giỏ hàng trên store
const stateGioHang = {
  gioHang: [],
};

const BaiTapGioHangReducer = (state = stateGioHang, action) => {
  switch (action.type) {
    case "THEM_GIO_HANG": {
      let index = state.gioHang.findIndex(
        (spGH) => spGH.maSP === action.spGioHang.maSP
      );
      if (index === -1) {
        state.gioHang.push(action.spGioHang);
      } else {
        state.gioHang[index].soLuong += 1;
      }
      // cập nhật lại state.gioHang
      state.gioHang = [...state.gioHang];
      // set state
      return { ...state };
    }
    case "XOA_GIO_HANG": {
      let gioHangCapNhat = [...state.gioHang];
      // tìm ra phần tử cần xóa dựa vào maSP
      let index = gioHangCapNhat.findIndex((spGH) => spGH.maSP === action.maSP);
      if (index !== -1) {
        gioHangCapNhat.splice(index, 1);
      }
      // cập nhật lại state giỏ hàng mới để component render lại
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    case "TANG_GIAM_SO_LUONG": {
      let gioHangCapNhat = [...state.gioHang];
      // xử lý tăng giảm trên giỏ hàng cập nhật
      let index = gioHangCapNhat.findIndex((spGH) => spGH.maSP === action.maSP);
      if (index !== -1) {
        let soLuong = gioHangCapNhat[index].soLuong + action.soLuong;
        gioHangCapNhat[index].soLuong = soLuong === 0 ? 1 : soLuong;
      }
      // lấy giá trị giỏ hàng cập nhật gán vào state.gioHang
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default BaiTapGioHangReducer;
