import BaiTapGioHangReducer from "./BaiTapGioHangReducer";
import BaiTapGameXucXacReducer from "./BaiTapXucXacReducer";

const { combineReducers } = require("redux");

const rootReducer = combineReducers({
  // store tổng của ứng dụng
  stateGioHang: BaiTapGioHangReducer, // state giỏ hàng
  BaiTapGameXucXacReducer, // state BaiTapGame
});

export default rootReducer;
