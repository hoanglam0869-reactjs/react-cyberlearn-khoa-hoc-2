const stateDefault = {
  taiXiu: true, // true: tài (11 -> 17), false: xỉu (từ 4 -> 10)
  mangXucXac: [
    { ma: 1, hinhAnh: "./img/gameXucXac/1.png" },
    { ma: 1, hinhAnh: "./img/gameXucXac/1.png" },
    { ma: 1, hinhAnh: "./img/gameXucXac/1.png" },
  ],
  soBanThang: 0,
  tongSoBanChoi: 0,
};

const BaiTapGameXucXacReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case "DAT_CUOC": {
      state.taiXiu = action.taiXiu;
      return { ...state };
    }
    case "PLAY_GAME": {
      // bước 1 xử lý random xúc xắc
      let mangXucXacNgauNhien = [];
      for (let i = 0; i < 3; i++) {
        // mỗi lần lặp random ra số ngẫu nhiên từ 1 -> 6
        let soNgauNhien = Math.floor(Math.random() * 6) + 1;
        // tạo ra 1 đối tượng xúc xắc từ số ngẫu nhiên
        let xucXacNgauNhien = {
          ma: soNgauNhien,
          hinhAnh: `./img/gameXucXac/${soNgauNhien}.png`,
        };
        // push vào mảng xúc xắc ngẫu nhiên
        mangXucXacNgauNhien.push(xucXacNgauNhien);
      }
      // gán state mangXucXac = mangXucXacNgauNhien
      state.mangXucXac = mangXucXacNgauNhien;
      // xử lý tăng bàn chơi
      state.tongSoBanChoi += 1;
      // xử lý số bàn thắng
      let tongSoDiem = mangXucXacNgauNhien.reduce((tongDiem, xucXac) => {
        return (tongDiem += xucXac.ma);
      }, 0);
      // xét điều kiện để người dùng thắng game
      if (tongSoDiem > 3 && tongSoDiem < 11 && !state.taiXiu) {
        state.soBanThang += 1;
      } else if (tongSoDiem > 10 && tongSoDiem < 18 && state.taiXiu) {
        state.soBanThang += 1;
      }
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default BaiTapGameXucXacReducer;
