import React, { Component } from "react";
// sử dụng thư viện connect để lấy dữ liệu từ store về
import { connect } from "react-redux";

class GioHangRedux extends Component {
  render() {
    return (
      <div
        className="modal fade"
        id="modalId"
        tabIndex={-1}
        data-bs-keyboard="false"
        role="dialog"
        aria-labelledby="modalTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" style={{ minWidth: 800 }}>
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Giỏ hàng</h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              />
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th>Mã SP</th>
                    <th>Hình ảnh</th>
                    <th>Tên sản phẩm</th>
                    <th>Giá</th>
                    <th>Số lượng</th>
                    <th>Thành tiền</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.gioHang.map((spGH, index) => {
                    return (
                      <tr key={index}>
                        <td>{spGH.maSP}</td>
                        <td>
                          <img
                            src={spGH.hinhAnh}
                            alt={spGH.hinhAnh}
                            width={50}
                          />
                        </td>
                        <td>{spGH.tenSP}</td>
                        <td>{spGH.gia.toLocaleString()}</td>
                        <td>
                          <button
                            className="btn btn-primary"
                            onClick={() =>
                              this.props.tangGiamSoLuong(spGH.maSP, -1)
                            }
                          >
                            -
                          </button>
                          {spGH.soLuong.toLocaleString()}
                          <button
                            className="btn btn-primary"
                            onClick={() =>
                              this.props.tangGiamSoLuong(spGH.maSP, 1)
                            }
                          >
                            +
                          </button>
                        </td>
                        <td>{(spGH.soLuong * spGH.gia).toLocaleString()}</td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => this.props.xoaGioHang(spGH.maSP)}
                          >
                            Xóa
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
                <tfoot>
                  <th colSpan={5}></th>
                  <th>Tổng tiền</th>
                  <th>
                    {this.props.gioHang
                      .reduce((tongTien, spGioHang, index) => {
                        return (tongTien += spGioHang.soLuong * spGioHang.gia);
                      }, 0)
                      .toLocaleString()}
                  </th>
                </tfoot>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// hàm lấy state redux biến đổi thành props của component
const mapStateToProps = (state) => {
  // state là state tổng của ứng dụng chứa các state con (rootReducer)
  return {
    gioHang: state.stateGioHang.gioHang,
  };
};

// hàm đưa dữ liệu lên reducer
const mapDispatchToProps = (dispatch) => {
  return {
    xoaGioHang: (maSP) => {
      // tạo action
      let action = {
        type: "XOA_GIO_HANG",
        maSP,
      };
      // dùng phương thức dispatch redux cung cấp để đưa dữ liệu lên reducer
      dispatch(action);
    },
    tangGiamSoLuong: (maSP, soLuong) => {
      // soLuong > 0 => xử lý tăng - soLuong < 0 => xử lý giảm
      // tạo action để đưa dữ liệu lên reducer
      let action = {
        type: "TANG_GIAM_SO_LUONG", // thuộc tính bắt buộc để biết chạy vào case nào trong tất cả reducer
        maSP,
        soLuong,
      };
      // đưa action lên reducer mỗi lần người dùng click vào
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GioHangRedux);
