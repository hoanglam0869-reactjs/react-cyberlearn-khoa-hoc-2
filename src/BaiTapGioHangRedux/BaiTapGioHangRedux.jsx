import React, { Component } from "react";
import ProductListRedux from "./ProductListRedux";
import GioHangRedux from "./GioHangRedux";
// import thư viện connect kết nối react component - redux store
import { connect } from "react-redux";

class BaiTapGioHangRedux extends Component {
  renderSoLuong = () => {
    return this.props.gioHang
      .reduce((tongSoLuong, spGH, index) => {
        return (tongSoLuong += spGH.soLuong);
      }, 0)
      .toLocaleString();
  };
  render() {
    return (
      <div className="container">
        <h3>Danh sách sản phẩm</h3>
        <div className="text-end">
          <span
            style={{ width: 17, cursor: "pointer" }}
            data-bs-toggle="modal"
            data-bs-target="#modalId"
          >
            <i className="fa fa-cart me-2">
              <i className="fa fa-cart-arrow-down"></i>
            </i>
            ({this.renderSoLuong()}) Giỏ hàng
          </span>
        </div>
        <ProductListRedux />
        <GioHangRedux />
      </div>
    );
  }
}

// viết hàm lấy giá trị state từ redux store và biến thành props component
const mapStateToProps = (state) => {
  return {
    gioHang: state.stateGioHang.gioHang,
  };
};

export default connect(mapStateToProps)(BaiTapGioHangRedux);
