import logo from "./logo.svg";
import "./App.css";
import BaiTapGioHangRedux from "./BaiTapGioHangRedux/BaiTapGioHangRedux";
import BaiTapGameXucXac from "./BaiTapXucXac/BaiTapGameXucXac";
import BaiTapOanTuXi from "./BaiTapOanTuXi/BaiTapOanTuXi";

function App() {
  return (
    <div className="App">
      {/* <BaiTapGioHangRedux /> */}
      {/* <BaiTapGameXucXac /> */}
      <BaiTapOanTuXi />
    </div>
  );
}

export default App;
