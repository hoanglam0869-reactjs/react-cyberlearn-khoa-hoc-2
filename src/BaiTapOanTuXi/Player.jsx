import React, { Component } from "react";

export default class Player extends Component {
  render() {
    return (
      <div className="player-game">
        <div className="think"></div>
        <div className="speech-bubble"></div>
        <img
          style={{ width: 250, height: 250 }}
          src="./img/gameOanTuXi/player.png"
          alt="Player"
        />
        <div className="row">
          <div className="col-4">
            <button className="btn-item">
              <img src="./img/gameOanTuXi/bao.png" alt="Bao" />
            </button>
          </div>
          <div className="col-4">
            <button className="btn-item">
              <img src="./img/gameOanTuXi/bua.png" alt="Bua" />
            </button>
          </div>
          <div className="col-4">
            <button className="btn-item">
              <img src="./img/gameOanTuXi/keo.png" alt="Keo" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}
