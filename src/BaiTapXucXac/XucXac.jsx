import React, { Component } from "react";
import { connect } from "react-redux";

class XucXac extends Component {
  renderXucXac = () => {
    // lấy props từ reducer về
    return this.props.mangXucXac.map((xucXac, index) => {
      return (
        <img
          key={index}
          className="ms-2"
          style={{ width: 50, height: 50 }}
          src={xucXac.hinhAnh}
          alt={xucXac.ma}
        />
      );
    });
  };

  render() {
    return <div>{this.renderXucXac()}</div>;
  }
}

// hàm lấy state từ redux về thành props của component
const mapStateToProps = (state) => {
  return {
    mangXucXac: state.BaiTapGameXucXacReducer.mangXucXac,
  };
};

export default connect(mapStateToProps)(XucXac);

/* <img
  className="mx-1"
  style={{ width: 50, height: 50 }}
  src="./img/gameXucXac/1.png"
  alt="1.png"
/>
<img
  className="mx-1"
  style={{ width: 50, height: 50 }}
  src="./img/gameXucXac/2.png"
  alt="2.png"
/>
<img
  className="mx-1"
  style={{ width: 50, height: 50 }}
  src="./img/gameXucXac/3.png"
  alt="3.png"
/> */
