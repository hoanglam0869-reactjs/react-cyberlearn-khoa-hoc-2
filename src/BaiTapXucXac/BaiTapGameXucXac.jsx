import React, { Component } from "react";
import "./BaiTapGameXucXac.css";
import XucXac from "./XucXac";
import ThongTinTroChoi from "./ThongTinTroChoi";
import { connect } from "react-redux";

class BaiTapGameXucXac extends Component {
  render() {
    return (
      <div className="game">
        <div className="title-game text-center mt-5 display-4">
          Bài tập game xúc xắc
        </div>
        <div className="row mt-5">
          <div className="col-5">
            <button
              className="btnGame"
              onClick={() => this.props.datCuoc(true)}
            >
              TÀI
            </button>
          </div>
          <div className="col-2">
            <XucXac />
          </div>
          <div className="col-5">
            <button
              className="btnGame"
              onClick={() => this.props.datCuoc(false)}
            >
              XỈU
            </button>
          </div>
        </div>
        <div>
          <ThongTinTroChoi />
          <button
            className="btn btn-success p-2 mt-5"
            onClick={() => this.props.playGame()}
          >
            Play game
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (taiXiu) => {
      // tạo action taiXiu
      let action = {
        type: "DAT_CUOC",
        taiXiu,
      };
      // gửi lên reducer
      dispatch(action);
    },
    playGame: () => {
      let action = {
        type: "PLAY_GAME",
      };
      // gửi dữ liệu type PLAY_GAME lên reducer
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(BaiTapGameXucXac);

// https://drive.google.com/drive/folders/1cFlPw51YzVfUa-c4FDQoEz6QL4V3O_Xx
